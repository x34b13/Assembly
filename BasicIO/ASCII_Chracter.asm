.MODEL SMALL 

.DATA
    X DB 67

.CODE
MAIN PROC 
    
    ; @DATA is a variable that holds the value of the location in memory where
    ; the data segment lives. It moves the memory location of @DATA into the AX
    ; register (16 bit register). The AX register is simply a temporarily place
    ; holder that you can load with values and perform execute commands against.
    ; Loading starting address of data segment in ax. 
    MOV AX , @DATA
    MOV DS, AX ; Data segment gets initialized.

    ; Waits for keyboard input from STDIN and echoes to STDOUT.    
    ; On return:
    ; AL = character from standard input device.
    MOV AH , 1
    INT 21H
   
    ; Store character from standard input device into BH. 
    MOV BH , AL
    
    ; Outputs character to STDOUT.
    ; DL = character to output.
    MOV AH , 2
    MOV DL , BH
    INT 21H

    ; Output carriage return to STDOUT.
    ; DL = character to output.
    MOV AH , 2
    MOV DL , 0DH
    INT 21H
   
    ; Output new line to STDOUT. 
    ; DL = character to output.
    MOV DL , 0AH
    INT 21H
    
    ; Output C to STDOUT. 
    ; DL = character to output.
    MOV AH , 2
    MOV DL , X
    INT 21H

    

    ; Approved method of program termination.
    MOV AH ,4CH
    INT 21H

; Marks the end of procedure name previously begun with PROC.
MAIN ENDP
END MAIN


